flash.ready(function() {

if(typeof search_settings === 'undefined') return;

    function $_GET(parameter) {
        var url = new URL(window.location.href);
        var p = url.searchParams.get(parameter);
        return p;
    }
    
    function serializeObject(obj) {
        var objectToFormData = function(model, form, namespace) {
            var formData = form || new FormData();
            var formKey;
            for(var propertyName in model) {
                if(!model.hasOwnProperty(propertyName) || !model[propertyName]) continue;
                var formKey = namespace ? namespace + '[' + propertyName + ']' : propertyName;
                if(model[propertyName] instanceof Date)
                    formData.append(formKey, model[propertyName].toISOString());
                else if(model[propertyName] instanceof Array) {
                    model[propertyName].forEach(function(element, index){
                        var tempFormKey = formKey + '[' + index + ']';
                        objectToFormData(element, formData, tempFormKey);
                    });
                }
                else if(typeof model[propertyName] === 'object' && !(model[propertyName] instanceof File))
                    objectToFormData(model[propertyName], formData, formKey);
                else
                    formData.append(formKey, model[propertyName].toString());
            }
            return formData;  
        };

        return param(obj);
    }

    var keywords = $_GET('keywords');
    keywords = decodeURIComponent(keywords).replace(/\+/g, '%20');
    if(keywords === 'null' || !keywords) return;

    var search = function(page) {
        var $container = document.querySelector('[data-search-results]');
        $container.classList.add('loading');
        var endpoint = 'https://vc6hn2s7g5.execute-api.eu-west-1.amazonaws.com/default/flashsearch';
        var data = {
            version: 'draft', // optional, it can be published or draft,
            token: search_settings.token, // base64 encoded storyblok token,
            sort_by: 'position:asc', // optional, default to position:asc
            page: page, //optional, default to all of the pages
            per_page: search_settings.limit, // default to 100
            conditions: [
                // these queries use the same syntax as storyblok stories api
                {
                    field: 'component',
                    operator: 'in',
                    value: 'Page, Standard Page Module'
                },
                // if you need to search a term
                {
                    operator: 'search_term',
                    value: keywords
                }
            ]
        };

        var xhr = new XMLHttpRequest();
        xhr.open('POST', endpoint, true);
        xhr.send(serializeObject(data));
        xhr.onload = function() {
            $container.classList.remove('loading');
            var data = JSON.parse(this.responseText);

            data.stories.forEach(function(story, index) {
                $container.innerHTML += search_settings.template(story, index, data.stories.length, keywords);
            });

            var $link = $container.querySelector('[data-search-load-more]');
            if($link !== null) {
                $container.removeChild($link);
            }
            $container.innerHTML += search_settings.load_more('data-search-load-more');
            $link = $container.querySelector('[data-search-load-more]');
            $link.addEventListener('click', function(e) {
                e.preventDefault();
                search(page + 1);
            }, false);

            if(page === 1 && !data.stories.length) $container.innerHTML = search_settings.no_results(keywords);
            if(page > 1 && !data.stories.length || data.stories.length < search_settings.limit) $container.removeChild($link);
        }
    };

    search(1);

});