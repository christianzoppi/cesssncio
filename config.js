/**
 * Site Configuration
 * ==================
 * You can use this configuration file everywhere
 * in your templates by using the config variable.
 * Example: <%= config.maps.google.api_key %>
 */
const js_core_version = '1.2.8'; // version of the current js framework
const timestamp = (new Date()).getTime();

module.exports = {

    sanbox_version: 1.1,
    timestamp: timestamp,

    // General Website Informations
    name: "Headless Flash",
    url: "https://www.togetherdigital.ie",
    tab_color: "#1779BA",

    // Storyblok Settings
    storyblok: {
        space_id: 40804,
        api_token: "BqYLQJzYRfQAmNpcCTydcAtt",
        folder: "storyblok",
        layout: "page",
        datasources: []
    },

    // Find & Replace
    replace: [
        // ['Replace This', 'By This'],
        // [/AlsoWorksWithRegex/gi, ':)'],
    ],

    // Modules without detail pages
    // i.e. "Team Module"
    modules_without_detail : [],
    modules_with_optional_detail_page: [],
    modules_with_optional_detail_page_without_fallback: [],

    // Managing XML and page sitemaps
    sitemap: {
        include_modules_common: ['Page', 'Folder', 'Standard Page Module'],
        include_modules_xml: [],
        include_modules_page: [],
        exclude_pages_common: ['settings', 'sitemap', 'homepage', 'almost-finished', 'thank-you', 'search'],
        exclude_pages_xml: ['privacy'],
        exclude_pages_page: [],
        exclude_folders: ['relationship']
    },

    // Critical CSS
    criticalcss : {
        include: []
    },

    // Maps APIs
    maps: {
        google: {
            api_key: "AIzaSyDCxmXh8p-WQYfiULY3a6nAK-AqWj_7_BI"
        }
    },

    // Recaptcha
    recaptcha: "6LcdnogUAAAAAJEGFie7rWPfAScZmcbJe57Uawhb",

    // Trackers APIs
    // (GTM, Woopra, etc)
    google_tag_manager: "",
    woopra: "",
    fonts: {
        google: [
            {
                "name": "Source Sans Pro",
                "sizes": "400,300,600"
            }
        ],
        typekit_id: '', //Ex. zms2zrz,
        custom: [] // Ex. ['faraco_handregular', 'other_font']
    },

    // IMGIX Settings
    imgix: {
        source: "togetherdigital.imgix.net",
        secure_url_token: "XCfRGqWFra7knft7",
        to_remove_from_url: "//a.storyblok.com/f/"
    },
    
    // Build Settings
    build: {
        folder: "build",
        include: [
            "assets",
            "robots.txt"
        ],
        exclude: [
            "assets/scss",
            "assets/js/plugins",
            "assets/js/polyfills",
            "assets/js/custom",
            "assets/js/flash"
        ]
    },

    // Server Watch settings
    watch: {
        build: [ // Here, if one of those files changes, site gets rebuilt
            "config.js",
            "data/**/*.json",
            "layouts/**/*.html",
            "pages/**/*.html",
            "snippets/**/*.html",
            "blocks/**/*.html"
        ],
        assets: [ // Whenever one of those files changes, assets get compiled
            "assets/scss/**/*.scss",
            "assets/js/plugins/*.js",
            "assets/js/custom/*.js",
            `assets/js/flash/${js_core_version}/core/**/*.js`,
            `assets/js/flash/${js_core_version}/libraries/*.js`,
            "blocks/**/style.scss",
            "blocks/**/script.js"
        ]
    },

    // Assets Compilation Settings
    assets_compile: {
        "assets/css/icons.min.css": [
            "assets/fonts/flaticon/_flaticon.scss"
        ],

        "assets/css/bundle.min.css": [
            "assets/scss/main.scss",
            "blocks/**/style.scss"
        ],

        "assets/js/bundle.min.js": [
            "assets/js/polyfills/*.js",
            `assets/js/flash/${js_core_version}/core/**/*.js`,
            `assets/js/flash/${js_core_version}/libraries/*.js`,
            `assets/js/flash/${js_core_version}/plugins/*.js`,
            "assets/js/plugins/*.js",
            "assets/js/custom/*.js",
            "blocks/**/script.js",
            `assets/js/flash/${js_core_version}/start.js`
        ]
    }
};