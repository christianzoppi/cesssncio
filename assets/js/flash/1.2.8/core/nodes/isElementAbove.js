/*!
 * Determine if an element is above the scrolled position
 * @param  {Node}    	element 	The element
 * @param  {Boolean}  	partially_above 	true if you want to check if even part of the element is above the viewport
 * @return {Boolean}  	true if the element is above the viewpoirt
 */
flashCore.prototype.isElementAbove = function (element, partially_above) {
	if(!element) {
		return;
	}
	
	var element_top = element.getBoundingClientRect().top;
	var element_height = element.getBoundingClientRect().height;
	if(partially_above) {
		return (element_top < 0 && (element_top + element_height) >= 0);
	} else {
		return ((element_top + element_height) < 0);
	}
};